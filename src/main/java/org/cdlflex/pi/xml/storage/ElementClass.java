package org.cdlflex.pi.xml.storage;

import java.util.*;
import org.w3c.dom.*;

/**
 *  Description of the class
 *
 * @author    Michael Sintek <Michael.Sintek@dfki.de>
 */
public class ElementClass {

    ElementClasses elementClasses;
    String name;
    boolean toplevel = false;
    boolean forcedAttribute = false;
    Hashtable<String, Boolean> attributes = new Hashtable<String, Boolean>();
    // value indicates cardinality
    HashMap<Node, String> elements = new HashMap<Node, String>();
    // of (Element|Text) -> id
    boolean multipleChildren = false;
    int idNo;


    public ElementClass(String name, boolean toplevel, ElementClasses elementClasses) {
        this.elementClasses = elementClasses;
        this.name = name;
        if (toplevel) {
            this.toplevel = true;
        }
    }

    void add(Node node) {
        elements.put(node, null);
        // null = no id yet
        Bag attributeCount = new Bag();
        // analyze children
        if (node instanceof Element) {
            // element attributes
            NamedNodeMap eAttributes = node.getAttributes();
            boolean idref = false;
            for (int i = 0; i < eAttributes.getLength(); i++) {
                Node attrNode = eAttributes.item(i);
                String attrName = attrNode.getNodeName();
                if (attrName.equals(Constants.ID)) {
                    String id = attrNode.getNodeValue();
                    elements.put(node, id);
                } else if (attrName.equals(Constants.IDREF)) {
                    idref = true;
                    String id = attrNode.getNodeValue();
                    elements.put(node, id);
                } else if (attrName.equals(Constants.ATTR)) {
                    forcedAttribute = true;
                } else {
                    // user defined element attribute
                    String eAttribute = "_" + attrName;
                    attributeCount.add(eAttribute);
                }
            }
            // child nodes
            NodeList childNodes = node.getChildNodes();
            if (idref && childNodes.getLength() > 0) {
                System.out.println("node with IDREF should not have children: " + node);
            }
            for (int i = 0; i < childNodes.getLength(); i++) {
                Node child = childNodes.item(i);
                String childElementName = elementClasses.collectClasses(child, false);
                if (childElementName != null) {
                    attributeCount.add(childElementName);
                }
            }
        } else if (node instanceof Text) {
            attributeCount.add(Constants.TextNodeName);
        }

        updateAttributes(attributes, attributeCount);
        if (attributeCount.size() > 1) {
            multipleChildren = true;
        }
    }

    void finish() {
        // generate missing ids
        idNo = 0;
        if (isClass()) {
            for (Iterator<Node> eIter = elements.keySet().iterator(); eIter.hasNext();) {
                Node node = (Node) eIter.next();
                String id = (String) elements.get(node);
                if (id == null) {
                    elements.put(node, newID());
                }
            }
        }
    }

    void generateAttributeInstanceSlots(String id, Node attributeChild, XMLFramesEvents fe) {
        String attributeName = attributeChild.getNodeName();
        NodeList grandChildren = attributeChild.getChildNodes();
        for (int i = 0; i < grandChildren.getLength(); i++) {
            Node grandChild = grandChildren.item(i);
            if (grandChild instanceof Element) {
                String grandChildName = grandChild.getNodeName();
                ElementClass ec = elementClasses.findClass(grandChildName);
                String id1 = (String) ec.elements.get(grandChild);
                fe.addInstanceSlotValue(id, attributeName, id1);
            }
        }
    }

    void generateInstance(String id, Node node, XMLFramesEvents fe) {
        if (node instanceof Text) {
            fe.newInstance(elementClasses.textName, id);
        } else if (node instanceof Element) {
            if (!isIdref((Element) node)) {
                fe.newInstance(getName(), id);
            }
        }
    }

    void generateInstances(XMLFramesEvents fe) {
        for (Iterator<Node> eIter = elements.keySet().iterator(); eIter.hasNext();) {
            Node node = (Node) eIter.next();
            String id = (String) elements.get(node);
            generateInstance(id, node, fe);
        }
    }

    void generateInstanceSlots(XMLFramesEvents fe) {
        for (Iterator<Node> eIter = elements.keySet().iterator(); eIter.hasNext();) {
            Node node = (Node) eIter.next();
            String id = (String) elements.get(node);
            generateInstanceSlots(id, node, fe);
        }
    }

    void generateInstanceSlots(String id, Node node, XMLFramesEvents fe) {
        if (node instanceof Text) {
            String text = node.getNodeValue().trim();
            fe.addStringSlotValue(id, elementClasses.textName, text);
        } else if (node instanceof Element) {
            if (isIdref((Element) node)) {
                return;
            }
            // element attributes
            NamedNodeMap eAttributes = node.getAttributes();
            for (int i = 0; i < eAttributes.getLength(); i++) {
                Node attrNode = eAttributes.item(i);
                String attrName = attrNode.getNodeName();
                if (attrName.equals(Constants.ID)) {
                    // ignore
                } else if (attrName.equals(Constants.ATTR)) {
                    // we must never get here!
                } else {
                    // user defined element attribute
                    String attributeName = "_" + attrName;
                    String attributeValue = attrNode.getNodeValue();
                    fe.addStringSlotValue(id, attributeName, attributeValue);
                }
            }
            // child nodes
            NodeList childNodes = node.getChildNodes();
            for (int i = 0; i < childNodes.getLength(); i++) {
                Node child = childNodes.item(i);
                String childName = child.getNodeName();
                String attributeName = getAttributeName(childName);
                
                if (childName.equals(Constants.TextNodeName)) {
                    String text = child.getNodeValue().trim();
                    if (!text.equals("")) {
                        fe.addStringSlotValue(id, attributeName, text);
                    }
                } else {
                    ElementClass ec = elementClasses.findClass(childName);
                    if (ec != null) {
                    	if (ec.isClass()) {
                        	String id1 = (String) ec.elements.get(child);
                        	fe.addInstanceSlotValue(id, attributeName, id1);
                    	} else {
                            // ec is attribute
                            if (ec.isSimpleAttribute()) {

                                /* -- @FJ: hard-modification 
                                 * need for adding datatype inside of ppx:type */
                                if(node.getNodeName().equalsIgnoreCase("ppx:type")) {
                                    fe.addInstanceSlotValue(id, "ppx_derivedSlot", attributeName);
                                }
                                /*    end of modification -- */
                                
                                Node grandChild = child.getFirstChild();
                                if (grandChild != null &&
                                    grandChild instanceof Text) {
                                    String text = grandChild.getNodeValue().trim();
                                    if (!text.equals("")) {
                                        fe.addStringSlotValue(id, attributeName, text);
                                    }
                                }
                            } else {
                                // complex attribute
                                generateAttributeInstanceSlots(id, child, fe);
                            }
                        }
                    }
                }
            }
        }
    }

    String getAttributeName(String attribute) {
        if (attribute.equals(Constants.TextNodeName)) {
            return elementClasses.textName;
        } else {
            ElementClass ec = elementClasses.findClass(attribute);
            if (ec != null && ec.isClass()) {
                return attribute + Constants.SlotSuffix;
            } else {
                return attribute;
            }
        }
    }

    Enumeration<String> getAttributes() {
        return attributes.keys();
    }

    Object getAttributeType(String attribute) {
        // returns class name, Collection of class names, or Constants.TextNodeName
        if (attribute.equals(Constants.TextNodeName)) {
            return Constants.TextNodeName;
        } else {
            ElementClass ec = elementClasses.findClass(attribute);
            if (ec != null && ec.isClass()) {
                return attribute;
            } else if (ec != null && ec.isForcedAttribute()) {
                return (Collection<?>) ec.attributes.keySet();
            } else {
                return Constants.TextNodeName;
            }
        }
    }

    String getName() {
        if (name.equals(Constants.TextNodeName)) {
            return elementClasses.textName;
        } else {
            return name;
        }
    }

    boolean isClass() {
        return toplevel || (!isForcedAttribute() && !isSimpleAttribute());
    }

    boolean isForcedAttribute() {
        return forcedAttribute;
    }

    boolean isIdref(Element element) {
        return element.getAttributeNode(Constants.IDREF) != null;
    }

    boolean isMultipleValued(String attribute) {
        ElementClass ec = elementClasses.findClass(attribute);
        if (ec != null && ec.isForcedAttribute()) {
            return ec.multipleChildren;
        } else {
            return ((Boolean) attributes.get(attribute)).booleanValue();
        }
    }

    boolean isSimpleAttribute() {
        return attributes.size() == 0
            || (attributes.size() == 1 && ((String) attributes.keys().nextElement()).equals(Constants.TextNodeName));
    }

    String newID() {
        String id = getName() + "_" + idNo;
        idNo++;
        if (elements.containsValue(id)) {
            return newID();
        } else {
            return id;
        }
    }

    void updateAttributes(Hashtable<String, Boolean> attributes, Bag attributeCount) {
        // adds new attributes and changes cardinalities
        for (Iterator<?> aIter = attributeCount.iterator(); aIter.hasNext();) {
            String attr = (String) aIter.next();
            Boolean isMultiple = (Boolean) attributes.get(attr);
            if (isMultiple == null) {
                // new attribute
                if (attributeCount.isMultiple(attr)) {
                    isMultiple = Boolean.TRUE;
                } else {
                    isMultiple = Boolean.FALSE;
                }
                attributes.put(attr, isMultiple);
            } else {
                // old attribute
                if (attributeCount.isMultiple(attr) && !isMultiple.booleanValue()) {
                    attributes.put(attr, Boolean.TRUE);
                }
            }
        }
    }
}
