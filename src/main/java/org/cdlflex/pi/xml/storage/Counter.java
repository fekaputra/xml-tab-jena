package org.cdlflex.pi.xml.storage;



class Counter {

    int counter;

    Counter() {
        counter = 1;
    }

    void inc() {
        counter++;
    }

    boolean isMultiple() {
        return counter > 1;
    }
}
