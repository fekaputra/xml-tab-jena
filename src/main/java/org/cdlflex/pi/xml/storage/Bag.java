package org.cdlflex.pi.xml.storage;

import java.util.*;

/**
 *  Description of the Class
 *
 *  @author    Michael Sintek <Michael.Sintek@dfki.de>
 */
public class Bag {

    HashMap<Object, Counter> bag = new HashMap<Object, Counter>();
    int count = 0;

    void add(Object object) {
        count++;
        Counter counter = (Counter) bag.get(object);
        if (counter == null) {
            counter = new Counter();
            bag.put(object, counter);
        } else {
            counter.inc();
        }
    }

    boolean contains(Object object) {
        return bag.containsKey(object);
    }

    boolean isMultiple(Object object) {
        Counter counter = (Counter) bag.get(object);
        if (counter != null) {
            return counter.isMultiple();
        } else {
            return false;
        }
    }

    Iterator<Object> iterator() {
        return bag.keySet().iterator();
    }

    int size() {
        return count;
    }
}
