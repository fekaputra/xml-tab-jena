package org.cdlflex.pi.xml.storage;

import java.util.*;

/**
 * Prints XML (to stdout). Not currently used.
 * More or less for debugging.
 *
 * @author    Michael Sintek <Michael.Sintek@dfki.de>
 */
public class PrintXMLFramesEvents implements XMLFramesEvents {

    public void addInstanceSlot(String className, String slotName, String type, boolean isMultipleValued) {
        System.out.println(
            "addInstanceSlot(" + className + ", " + slotName + " : " + type + (isMultipleValued ? " ..." : "") + ")");
    }

    public void addInstanceSlot(String className, String slotName, Collection<?> types, boolean isMultipleValued) {
        System.out.println(
            "addInstanceSlot(" + className + ", " + slotName + " : " + types + (isMultipleValued ? " ..." : "") + ")");
    }

    public void addInstanceSlotValue(String instanceID, String slotName, String valueInstanceID) {
        System.out.println("addInstanceSlotValue(" + instanceID + "." + slotName + " = " + valueInstanceID + ")");
    }

    public void addStringSlot(String className, String slotName, boolean isMultipleValued) {
        System.out.println("addStringSlot(" + className + ", " + slotName + (isMultipleValued ? " ..." : "") + ")");
    }

    public void addStringSlotValue(String instanceID, String slotName, String value) {
        System.out.println("addStringSlotValue(" + instanceID + "." + slotName + " = \"" + value + "\")");
    }

    public void newClass(String name) {
        System.out.println("newClass(" + name + ")");
    }

    public void newInstance(String className, String instanceID) {
        System.out.println("newInstance(" + instanceID + " : " + className + ")");
    }
}
