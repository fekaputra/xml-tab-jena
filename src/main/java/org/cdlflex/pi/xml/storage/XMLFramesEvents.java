package org.cdlflex.pi.xml.storage;

import java.util.*;

/**
 *  Description of the Interface
 *
 *  @author    Michael Sintek <Michael.Sintek@dfki.de>
 */
public interface XMLFramesEvents {

    public void addInstanceSlot(String className, String slotName, String type, boolean isMultipleValued);

    public void addInstanceSlot(String className, String slotName, Collection<?> types, boolean isMultipleValued);

    public void addInstanceSlotValue(String instanceID, String slotName, String valueInstanceID);

    public void addStringSlot(String className, String slotName, boolean isMultipleValued);

    public void addStringSlotValue(String instanceID, String slotName, String value);

    public void newClass(String name);

    public void newInstance(String className, String instanceID);
}
