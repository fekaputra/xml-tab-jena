package org.cdlflex.pi.xml.storage;

import java.io.File;
import java.util.Collection;
import java.util.Enumeration;
import java.util.Hashtable;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.cdlflex.pi.xml.main.XMLImport;
import org.w3c.dom.Comment;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.w3c.dom.Text;

import org.xml.sax.SAXException;
import org.xml.sax.SAXParseException;

/**
 *  Description of the class
 *
 * @author    Michael Sintek <Michael.Sintek@dfki.de>
 */
public class ElementClasses {

    String fileName;
    //XmlDocument doc;
    Document doc;
    Element root;
    Hashtable<String, ElementClass> elementClasses = new Hashtable<String, ElementClass>();
    // of ElementClass
    String textName;


    public ElementClasses(String fileName) {
        this.fileName = fileName;
    }

    String add(Node node, boolean toplevel) {
        String name = node.getNodeName();
        ElementClass ec = findClass(name);
        if (ec == null) {
            ec = new ElementClass(name, toplevel, this);
            elementClasses.put(name, ec);
        }
        ec.add(node);
        return name;
    }

    void collectClasses() {
        NodeList toplevelNodes = root.getChildNodes();
        for (int i = 0; i < toplevelNodes.getLength(); i++) {
            collectClasses(toplevelNodes.item(i), true);
        }
    }

    String collectClasses(Node node, boolean toplevel) {
        // returns element class name (or null)
        if (node instanceof Element) {
            return add(node, toplevel);
        } else if (node instanceof Text) {
            if (!node.getNodeValue().trim().equals("")) {
                if (toplevel) {
                    return add(node, toplevel);
                } else {
                    return Constants.TextNodeName;
                }
            } else {
                return null;
            }
        } else if (node instanceof Comment) {
            return null;
        } else {
            System.out.println("node type unsupported: " + node.getNodeName());
            return null;
        }
    }

    public void createKnowledgeBase(XMLFramesEvents fe) {

        // test
        /*
         * XMLFramesEvents fep = new PrintFramesEvents();
         * iterateClasses(fep);
         * iterateInstances(fep);
         */

        iterateClasses(fe);
        XMLImport.end = System.currentTimeMillis();
        System.out.println("iterate classes: " + Long.toString(XMLImport.end - XMLImport.start));
        iterateInstances(fe);
        XMLImport.end = System.currentTimeMillis();
        System.out.println("iterate instances: " + Long.toString(XMLImport.end - XMLImport.start));

    }

    ElementClass findClass(String name) {
        return (ElementClass) elementClasses.get(name);
    }

    void findTextName(String name) {
        if (elementClasses.containsKey(name)) {
            findTextName(name + "_");
        } else {
            textName = name;
        }
    }

    void finish() {
        findTextName(Constants.TEXT);
        for (Enumeration<ElementClass> cEnum = elementClasses.elements(); cEnum.hasMoreElements();) {
            ElementClass elementClass = (ElementClass) cEnum.nextElement();
            elementClass.finish();
        }
    }

    public void iterateClasses(XMLFramesEvents fe) {

        // newClass events
        for (Enumeration<ElementClass> cEnum = elementClasses.elements(); cEnum.hasMoreElements();) {
            ElementClass elementClass = (ElementClass) cEnum.nextElement();
            if (elementClass.isClass()) {
                fe.newClass(elementClass.getName());
            }
        }
        XMLImport.end = System.currentTimeMillis();
        System.out.println("iterate classes (newclass): " + Long.toString(XMLImport.end - XMLImport.start));

        // add*Slot events
        for (Enumeration<ElementClass> cEnum = elementClasses.elements(); cEnum.hasMoreElements();) {
            ElementClass elementClass = (ElementClass) cEnum.nextElement();
            if (elementClass.isClass()) {
                String className = elementClass.getName();
                for (Enumeration<?> aEnum = elementClass.getAttributes(); aEnum.hasMoreElements();) {
                    String attribute = (String) aEnum.nextElement();
                    boolean isMultipleValued = elementClass.isMultipleValued(attribute);
                    String attributeName = elementClass.getAttributeName(attribute);
                    Object type = elementClass.getAttributeType(attribute);
                    if (type.equals(Constants.TextNodeName)) {
                        // string valued
                        
                        /* -- @FJ: hard-modification 
                         * Needed for ignoring all ppx_type value */
                        if(className.equalsIgnoreCase(Constants.PpxType)) continue;
                        /*    end of modification -- */
                        
                        fe.addStringSlot(className, attributeName, isMultipleValued);
                    } else if (type instanceof String) {
                        fe.addInstanceSlot(className, attributeName, (String) type, isMultipleValued);
                    } else {
                        fe.addInstanceSlot(className, attributeName, (Collection<?>) type, isMultipleValued);
                    }
                }
            }
        }
        XMLImport.end = System.currentTimeMillis();
        System.out.println("iterate classes (addslot): " + Long.toString(XMLImport.end - XMLImport.start));
    }

    public void iterateInstances(XMLFramesEvents fe) {

        // newInstance events
        for (Enumeration<ElementClass> cEnum = elementClasses.elements(); cEnum.hasMoreElements();) {
            ElementClass elementClass = (ElementClass) cEnum.nextElement();
            if (elementClass.isClass()) {
                elementClass.generateInstances(fe);
            }
        }
        XMLImport.end = System.currentTimeMillis();
        System.out.println("iterate instances (newInstance): " + Long.toString(XMLImport.end - XMLImport.start));

        // add*SlotValue events
        for (Enumeration<ElementClass> cEnum = elementClasses.elements(); cEnum.hasMoreElements();) {
            ElementClass elementClass = (ElementClass) cEnum.nextElement();
            if (elementClass.isClass()) {
                elementClass.generateInstanceSlots(fe);
            }
        }
        XMLImport.end = System.currentTimeMillis();
        System.out.println("iterate instances (SlotValue): " + Long.toString(XMLImport.end - XMLImport.start));
    }

    public static void main(String argv[]) {
        if (argv.length != 1) {
            System.err.println("Usage: cmd filename");
            System.exit(1);
        }
        ElementClasses ec = new ElementClasses(argv[0]);
        ec.createKnowledgeBase(new PrintXMLFramesEvents());
    }

    public String parse() {
        try {
            DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
            DocumentBuilder builder = factory.newDocumentBuilder();
            doc = builder.parse(new File(fileName));
            root = doc.getDocumentElement();
            root.normalize();
        } catch (SAXParseException err) {
            return "*** Parsing error" + ", line " + err.getLineNumber() + ", uri " + err.getSystemId() + ":\n" + err.getMessage();
        } catch (SAXException e) {
            Exception x = e.getException();
            return ((x == null) ? e.toString() : x.toString());
        } catch (Throwable t) {
            return t.toString();
        }

        collectClasses();
        finish();
        return null;
    }
}
