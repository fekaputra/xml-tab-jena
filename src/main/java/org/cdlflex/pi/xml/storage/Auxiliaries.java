package org.cdlflex.pi.xml.storage;

/**
 *  Description of the class
 *
 *  @author    Michael Sintek <Michael.Sintek@dfki.de>
 */
public class Auxiliaries {

    static String getNormalizedElementName(String name) {
        StringBuffer result = new StringBuffer();
        if (name == null || name == "") {
            name = "empty";
        }
        // should never happen
        for (int i = 0; i < name.length(); i++) {
            char c = name.charAt(i);
            if (Character.isLetterOrDigit(c)) {
                if (i == 0 && Character.isDigit(c)) {
                    result.append("_");
                }
                // names may not start with a digit
                result.append(c);
            } else {
                result.append('_');
            }
        }
        return result.toString();
    }
}
