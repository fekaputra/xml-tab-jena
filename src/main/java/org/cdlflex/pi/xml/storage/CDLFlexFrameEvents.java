package org.cdlflex.pi.xml.storage;

import java.util.Collection;
import java.util.Iterator;

import com.hp.hpl.jena.ontology.DatatypeProperty;
import com.hp.hpl.jena.ontology.ObjectProperty;
import com.hp.hpl.jena.ontology.OntClass;
import com.hp.hpl.jena.ontology.OntModel;
import com.hp.hpl.jena.ontology.OntResource;

/**
 * 
 * 
 * @author Juang
 */
public class CDLFlexFrameEvents implements XMLFramesEvents {
    OntModel model;
    
    public CDLFlexFrameEvents(OntModel model) {
        this.model = model;
    }

    public void addInstanceSlot(String className, String slotName, String type, boolean isMultipleValued) {
        ObjectProperty prop = model.createObjectProperty(defaultURI(slotName), false);
        prop.addDomain(model.createClass(defaultURI(className)));
        prop.addRange(model.createClass(defaultURI(type)));
    }

    public void addInstanceSlot(String className, String slotName, Collection<?> types, boolean isMultipleValued) {
        ObjectProperty prop = model.createObjectProperty(defaultURI(slotName), false);
        prop.addDomain(model.createClass(defaultURI(className)));
        for (Iterator<?> iter = types.iterator(); iter.hasNext();) {
            String type = (String) iter.next();
            prop.addRange(model.createClass(defaultURI(type)));
        }
    }

    public void addInstanceSlotValue(String instanceID, String slotName, String valueInstanceID) {
        ObjectProperty prop = model.createObjectProperty((defaultURI(slotName)), false);
        OntResource instance = model.createOntResource(defaultURI(instanceID));
        OntResource propValue = model.createOntResource(defaultURI(valueInstanceID));
        model.add(instance, prop, propValue);
    }

    public void addStringSlot(String className, String slotName, boolean isMultipleValued) {
        DatatypeProperty prop = model.createDatatypeProperty(defaultURI(slotName), false);
        prop.addDomain(model.createClass(defaultURI(className)));
    }

    public void addStringSlotValue(String instanceID, String slotName, String value) {
        DatatypeProperty prop = model.createDatatypeProperty((defaultURI(slotName)), false);
        OntResource instance = model.createOntResource(defaultURI(instanceID));
        model.add(instance, prop, value);
    }

    public void newClass(String name) {
        model.createClass(defaultURI(name));
    }

    public void newInstance(String className, String instanceID) {
        OntClass concept = model.createClass(defaultURI(className));
        model.createIndividual(defaultURI(instanceID), concept);
    }
    
    //auxilary functions
    private String defaultURI(String name) {
        StringBuffer sb = new StringBuffer();
        sb.append(model.getNsPrefixURI("")).append(Auxiliaries.getNormalizedElementName(name));
        return sb.toString();
    }
}
