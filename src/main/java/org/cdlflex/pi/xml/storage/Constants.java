package org.cdlflex.pi.xml.storage;

class Constants {

    final static String TextNodeName = "#text";

    final static String TEXT = "Text";
    final static String SlotSuffix = "Slot";

    // XML element attributes
    final static String ID = "p_id";
    final static String IDREF = "p_idref";
    final static String ATTR = "p_attr";
    
    // type--customized
    final static String PpxType = "ppx:type"; 
}
