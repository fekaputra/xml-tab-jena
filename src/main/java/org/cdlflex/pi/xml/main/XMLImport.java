package org.cdlflex.pi.xml.main;

import org.cdlflex.pi.xml.helper.JenaHelper;
import org.cdlflex.pi.xml.storage.CDLFlexFrameEvents;
import org.cdlflex.pi.xml.storage.ElementClasses;
import org.cdlflex.pi.xml.storage.XMLFramesEvents;

import com.hp.hpl.jena.ontology.OntModel;

public class XMLImport {
    public static long start;
    public static long end;
    OntModel model;
//    String name = "HydTest_Shanghai_V10_HC.esm";
    String name = "HYDTEST.L2P.wze";
    
    public XMLImport() {
//        model = JenaHelper.readFile("owl/template__.owl");
        model = JenaHelper.readFile("owl/template_.owl");
    }
    
    private void exportKb() {
        JenaHelper.writeFile(model, name+".owl");
        XMLImport.end = System.currentTimeMillis();
        System.out.println("Total+Export: " + Long.toString(XMLImport.end - start));
    }
    
    // various actions:
    private void importKb() {
        String filename = "owl/"+name+".xml";
        
        if (filename.equals("")) {
            return;
        }
        // check if exists ...
        System.out.println("Importing kb ...");
        ElementClasses ec = new ElementClasses(filename);
        XMLImport.start = System.currentTimeMillis();
        String error = ec.parse();
        XMLImport.end = System.currentTimeMillis();
        System.out.println("EC Parse: " + Long.toString(XMLImport.end - start));
        if (error == null) {
            XMLFramesEvents pxfe = new CDLFlexFrameEvents(model);
            if (pxfe != null) {
                ec.createKnowledgeBase(pxfe);
                XMLImport.end = System.currentTimeMillis();
                System.out.println("Total: " + Long.toString(XMLImport.end - start));
            }

            System.out.println("Import successful.");
        } else {
            System.out.println("There were errors at import.\n" +
                    "See console for details.");
        }
        System.out.println("Import done.");
        
    }
    
    public static void main(String[] args) {
        XMLImport xmlImport = new XMLImport();
        xmlImport.importKb();
        xmlImport.exportKb();
    }
}
