package org.cdlflex.pi.xml.helper;

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;

import com.hp.hpl.jena.ontology.OntClass;
import com.hp.hpl.jena.ontology.OntModel;
import com.hp.hpl.jena.rdf.model.ModelFactory;
import com.hp.hpl.jena.util.FileManager;

public class JenaHelper {
    
    public static OntModel readFile(String URL) {
        OntModel model = ModelFactory.createOntologyModel();
        try {
            InputStream in = FileManager.get().open(URL);
            if(in==null) throw new IllegalArgumentException("File: '"+URL+"' not found");
            model.read(in, null);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return model;
    }
    public static void writeFile(OntModel model, String URL) {
        
        try {
            FileOutputStream fileOut = new FileOutputStream(URL);
            model.write(fileOut);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    public static String defaultNS(OntModel model) {
        return model.getNsPrefixURI("");
    }
    public static OntClass getOwlThingClass(OntModel model) {
        return model.getOntClass(model.getNsPrefixURI("owl")+"Thing");
    }
}
