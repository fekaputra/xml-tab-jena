# README #

### What is this repository for? ###

XMLTabJena is a data transformation engine from any XML-based file format to OWL. 
Adapted from Protégé plugin called XMLTab (http://protegewiki.stanford.edu/wiki/XML_Tab) using the latest Apache Jena (http://jena.apache.org) engine and re-created into a stand-alone application.

### Who do I talk to? ###

* Repo owner or admin: Fajar Ekaputra (fajar.ekaputra@tuwien.ac.at)